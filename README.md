# welearntogether

Anggota welearntogether
- [Hi]I'm Akbarrr [link to branch](https://gitlab.com/akb4r5yn/welearntogether/-/tree/akbar),
- [Hi]I'm Mairaaa [link to branch](https://gitlab.com/akb4r5yn/welearntogether/-/tree/maira)

## About Git

### Fitur Fitur
Berikut adalah fitur-fitur unggulan dari GIT antara lain:

1. Version control system yang terdistribusi, GIT menggunakan pendekatan peer to peer, tidak seperti yang lainnya seperti Subversion (SVN) yang menggunakan model client-server.

2. GIT memungkinkan developer untuk memiliki branch kode yang independent dan massive. Membuat, menghapus dan menggabungkan branch tersebut menjadi lebih cepat, lancar dan tidak butuh waktu yang lama.

3. Dalam GIT, semua operasinya bersifat atomic, artinya sebuah tindakan akan benar-benar diselesaikan dengan lengkap atau sama sekali gagal. Ini sangat penting, karena di beberapa version control system seperti CVS, operasinya bersifat non-atomic. Jika ada operasi yang ‘gantung’ dan terkait dengan repository, kondisi repository menjadi tidak stabil.

4. Dalam GIT, semuanya disimpan dalam folder .git. Berbeda dengan VCS lain seperti SVN atau CVS, dimana metadata file disimpan dalam folder tersembunyi seperti .cvs, .svn, .etc.

5. GIT menggunakan data model yang membanti memastikan integritas cryptographic apapun ada dalam repository. Setiap kali sebuah file ditambahkan atau di-commit, checksum-nya akan diciptakan; sama hal nya, juga di-retrieve lewat checksum-nya juga.

6. Fitur menarik lainnya yang ada di GIT adalah staging area atau index. Dengan stagin area, developer bisa memformat commit dan membuatnya bisa di-review sebelum benar-benar diterapkan.

## Cara Menggunakan Git
Berikut adalah basic dasar dalam menggunaka git yang harus kita ketahui di antaranya yaitu ;

1. Git init
Jadi git init atau bisa dibilang _initialitation_ adalah perintah awal untuk memulai sebuah repository baru pada sebuah direktori. ketika kita menjalankan perintah **git init** pada terminal git maka otomatis program git akan membuat sebuah file direktori yang bernama _.git_.
Setelah Anda menjalankan perintah git init, direktori tersebut akan menjadi repositori Git, dan Anda dapat mulai menambahkan file ke dalam repositori, melakukan commit, dan melakukan operasi Git lainnya.

2. Git clone / (Git add, Git push, Git commit)
Jadi untuk langkah kedua setelah kita melaukan git init sebenarnya kita bisa melakukan Git clone maupun Git add, push, and commit, Tergantung dari situasi kedua repostiori yang ada di lokal dan yang ada di gitlabnya sendiri. Dan berikut adalah deskripsi masing-masing mengenai git clone, git add, git push, dan git commit:
    * Git clone adalah perintah pada Git untuk mengambil salinan lengkap dari sebuah repositori yang ada di server dan menyimpannya di komputer lokal. Dengan menggunakan git clone, Anda dapat membuat salinan lokal dari repositori Git yang terdapat di server. Setelah repositori berhasil di-clone, Anda dapat melakukan operasi Git seperti commit, push, dan pull pada repositori tersebut. Untuk menjalankan git clone, Anda perlu menentukan URL repositori yang ingin Anda clone dan kemudian menjalankan perintah di terminal atau command prompt. Contohnya seperti berikut: ```git clone https://github.com/nama-akun/nama-repo.git```
    
    * Git add adalah perintah pada Git yang digunakan untuk menambahkan perubahan pada file ke dalam index Git sebelum melakukan commit. Saat Anda melakukan perubahan pada file dalam repositori Git, perubahan tersebut tidak langsung disimpan dalam commit. Anda perlu menambahkan perubahan tersebut ke dalam index Git terlebih dahulu sebelum melakukan commit. Proses ini disebut dengan proses staging. git add digunakan untuk menambahkan perubahan pada file ke dalam index Git. Anda dapat menambahkan perubahan pada satu atau beberapa file atau seluruh file yang berubah dengan menggunakan perintah git add.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/akb4r5yn/welearntogether.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/akb4r5yn/welearntogether/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
